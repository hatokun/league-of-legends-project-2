/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import champions.*;
import interfaces.Unit;

/**
 *
 * @author ismael tesisteco
 */
public class FactoryUnits {
    
    private static final FactoryUnits FACTORY = new FactoryUnits();
    
    private FactoryUnits(){}
    
    public static FactoryUnits getFactory(){    
        return FactoryUnits.FACTORY;
    }
    
    public Unit getUnit(String nameUnit){
        
        switch (nameUnit) {
            case "Janna":
                return new Janna();             
            case "Morgana":
                return new Morgana();
            case "Zed":
                return new Zed();               
            default:
                throw new AssertionError();
        }
    
    }
    
}
