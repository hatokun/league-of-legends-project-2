/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

/**
 *
 * @author ismael tesisteco
 */
public class Statistics {
    private double base_health;
    private double base_health_regen;
    private double base_mana;
    private double base_mana_regen;
    private double base_energy;
    private double base_energy_regen;
    private double base_attack_damage;
    private double base_attack_speed;
    private double base_armor;
    private double base_magic_resistence;
    private double base_movent_speed;
    /**/
    private double health;
    private double actual_health;
    private double health_regen;
    private double mana;
    private double mana_regen;
    private double energy;
    private double energy_regen;
    private double attack_damage;
    private double attack_speed;
    private double armor;
    private double magic_resistence;
    private double movent_speed;/**/    
        
    private double health_per_lv;
    private double health_regen_per_lv;
    private double mana_per_lv;
    private double energy_per_lv;
    private double mana_regen_per_lv;
    private double attack_damage_per_lv;
    private double attack_speed_per_lv;
    private double armor_per_lv;
    private double magic_resistence_per_lv;
    private double movent_speed_per_lv;  
    
    private boolean isMelee;

    public Statistics() {
        this.base_health = 0;
        this.base_health_regen = 0;
        this.base_mana = 0;
        this.base_energy = 0;
        this.base_mana_regen = 0;
        this.base_attack_damage = 0;
        this.base_attack_speed = 0;
        this.base_armor = 0;
        this.base_magic_resistence = 0;
        this.base_movent_speed = 0;
        this.health_per_lv = 0;
        this.health_regen_per_lv = 0;
        this.mana_per_lv = 0;
        this.energy_per_lv = 0;
        this.mana_regen_per_lv = 0;
        this.attack_damage_per_lv = 0;
        this.attack_speed_per_lv = 0;
        this.armor_per_lv = 0;
        this.magic_resistence_per_lv = 0;
        this.movent_speed_per_lv = 0;
        this.isMelee = false;       
        this.level = 1;
        this.time = 0;
    }

    public boolean isMelee() {
        return isMelee;
    }

    public void setMelee() {
        this.isMelee = true;
    }

    public void setActual_health(double actual_health) {        
        this.actual_health = actual_health;
    }

    public double getActual_health() {
        return actual_health;
    }


    

    //Champion Level
    public void setLevel(int level) {
        this.level = level;
        this.actual_health = this.getHealth();
    }
    
    public void setHealth(double health) {
        this.base_health = health;
    }

    public void setHealth_regen(double health_regen) {
        this.base_health_regen = health_regen;
    }

    public void setMana(double mana) {
        this.base_mana = mana;
    }

    public void setMana_regen(double mana_regen) {
        this.base_mana_regen = mana_regen;
    }
    
    public void setEnergy(double energy) {
        this.base_energy = energy;
    }

    public void setEnergy_regen(double energy_regen) {
        this.base_energy_regen = energy_regen;
    }    

    public void setAttack_damage(double attack_damage) {
        this.base_attack_damage = attack_damage;
    }

    public void setAttack_speed(double attack_speed) {
        this.base_attack_speed = attack_speed;
    }

    public void setArmor(double armor) {
        this.base_armor = armor;
    }

    public void setMagic_resistence(double magic_resistence) {
        this.base_magic_resistence = magic_resistence;
    }

    public void setMovent_speed(double movent_speed) {
        this.base_movent_speed = movent_speed;
    }

    public void setHealth_per_lv(double health_per_lv) {
        this.health_per_lv = health_per_lv;
    }

    public void setHealth_regen_per_lv(double health_regen_per_lv) {
        this.health_regen_per_lv = health_regen_per_lv;
    }

    public void setMana_per_lv(double mana_per_lv) {
        this.mana_per_lv = mana_per_lv;
    }

    public void setMana_regen_per_lv(double mana_regen_per_lv) {
        this.mana_regen_per_lv = mana_regen_per_lv;
    }

    public void setAttack_damage_per_lv(double attack_damage_per_lv) {
        this.attack_damage_per_lv = attack_damage_per_lv;
    }

    public void setAttack_speed_per_lv(double attack_speed_per_lv) {
        this.attack_speed_per_lv = attack_speed_per_lv;
    }

    public void setArmor_per_lv(double armor_per_lv) {
        this.armor_per_lv = armor_per_lv;
    }

    public void setMagic_resistence_per_lv(double magic_resistence_per_lv) {
        this.magic_resistence_per_lv = magic_resistence_per_lv;
    }

    public void setMovent_speed_per_lv(double movent_speed_per_lv) {
        this.movent_speed_per_lv = movent_speed_per_lv;
    }

    public void setTime(double time) {
        this.time = time;
    }
  
    
    private int level;
    private double time;

    public double getHealth() {
        return base_health + health_per_lv * level;
    }

    public double getHealth_regen() {
        return base_health_regen + health_regen_per_lv * level;
    }

    public double getMana() {
        return base_mana + mana_per_lv * level;
    }
    
    public double getEnergy() {
        return base_energy + energy_per_lv * level;
    }

    public double getMana_regen() {
        return base_mana_regen + mana_regen_per_lv * level;
    }

    public double getAttack_damage() {
        return base_attack_damage + attack_damage_per_lv * level;
    }

    public double getAttack_speed() {
        return base_attack_speed + attack_speed_per_lv * level;
    }

    public double getArmor() {
        return base_armor + armor_per_lv * level;
    }

    public double getMagic_resistence() {
        return base_magic_resistence + magic_resistence_per_lv * level;
    }

    public double getMovent_speed() {
        return base_movent_speed + movent_speed_per_lv * level;
    }

    public int getLevel() {
        return level;
    }

    public double getTime() {
        return time;
    }
       
}
