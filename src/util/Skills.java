/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

/**
 *
 * @author ismael tesisteco
 */
public class Skills {
    
    private int lvQ;
    private int lvW;
    private int lvE;
    private int lvR;    
    
    public void setlvQ(int lvQ){
        this.lvQ = lvQ;
    }

    public int getlvQ() {
        return lvQ;
    }

    public int getlvW() {
        return lvW;
    }

    public int getlvE() {
        return lvE;
    }

    public int getlvR() {
        return lvR;
    }
    
    public void setlvW(int lvW){
        this.lvW = lvW;
    }
    
    public void setlvE(int lvE){
        this.lvE = lvE;
    }
    
    public void setlvR(int lvR){
        this.lvR = lvR;
    }   
    
    
}
