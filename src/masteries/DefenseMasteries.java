/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package masteries;

/**
 *
 * @author ismael tesisteco
 */
public class DefenseMasteries{
    enum index {BLOCK, RECOVERY, ENCHANTED_ARMOR, TOUGH_SKIN, UNYIELDING, VETERAN_SKIN, 
                BLADE_ARMOR, OPPRESION, JUGGERNAUT, HARDINESS, RESISTENCE, PERSEVERANCE,
                SWIFTNESS, REINFORCED_ARMOR, EVASIVE, SECOND_WIND, LEGENDARY_GUARDIAN, 
                RUNIC_BLESSING, TENACIOUS};

    private String masteries_points[];

    public DefenseMasteries() {
        this.masteries_points = new String[20];
        for (int i = 0; i < 20; i++) {
            this.masteries_points[i] = "0";
        }
    }

    public void setMasteries_points(String[] masteries_points) {
        this.masteries_points = masteries_points;
    }   
/*
    public int getBlock() {
        return block;
    }

    public int getRecovery() {
        return recovery;
    }

    public int getEnchanted_armor() {
        return enchanted_armor;
    }

    public double getTough_skin() {
        return 0;
    }/**/

    public double getUnyielding(boolean melee) {
        int value = Integer.parseInt(masteries_points[index.UNYIELDING.ordinal()]);
        return melee ? 2 : 1;
    }
/**
    public int getVeteran_skin() {
        return veteran_skin;
    }

    public int getBlade_armor() {
        return blade_armor;
    }

    public int getOppresion() {
        return oppresion;
    }

    public int getJuggernaut() {
        return juggernaut;
    }

    public int getHardiness() {
        return hardiness;
    }

    public int getResistence() {
        return resistence;
    }

    public int getPerseverance() {
        return perseverance;
    }

    public int getSwiftness() {
        return swiftness;
    }

    public int getReinforced_armor() {
        return reinforced_armor;
    }

    public int getEvasive() {
        return evasive;
    }

    public int getSecond_wind() {
        return second_wind;
    }

    public int getLegendary_guardian() {
        return legendary_guardian;
    }

    public int getRunic_blessing() {
        return runic_blessing;
    }

    public int getTenacious() {
        return tenacious;
    }
/**/
    public String[] getMasteries_points() {
        return masteries_points;
    } 
}
