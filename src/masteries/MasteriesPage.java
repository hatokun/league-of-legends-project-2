/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package masteries;

/**
 *
 * @author ismael tesisteco
 */
public class MasteriesPage {
    private OffenseMasteries offenseMasteries;
    private DefenseMasteries defenseMasteries;
    private UtilityMasteries utilityMasteries;    

    public OffenseMasteries getOffenseMasteries() {
        return offenseMasteries;
    }

    public void setOffenseMasteries(OffenseMasteries offenseMasteries) {
        this.offenseMasteries = offenseMasteries;
    }

    public DefenseMasteries getDefenseMasteries() {
        return defenseMasteries;
    }

    public void setDefenseMasteries(DefenseMasteries defenseMasteries) {
        this.defenseMasteries = defenseMasteries;
    }

    public UtilityMasteries getUtilityMasteries() {
        return utilityMasteries;
    }

    public void setUtilityMasteries(UtilityMasteries utilityMasteries) {
        this.utilityMasteries = utilityMasteries;
    }
}
