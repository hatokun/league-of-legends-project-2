/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package masteries;

/**
 *
 * @author ismael tesisteco
 */
public class OffenseMasteries {
    
    enum index {DOUBLE_EDGE, FURY, SORCERY, BUTCHER, EXPOSE_WEAKNESS, BRUTE_FORCE,
                MENTAL_FORCE, FEAST, SPELL_WEAVING, MARTIAL_MASTERY, ARCANE_MASTERY,
                EXECUTIONER, BLADE_WEAVING, WARLORD, ARCHEMAGE, DANGEORUS_GAME, 
                FRENZY, DEVASTING_STRIKE, ARCANE_BLADE, HAVOC};

    private String masteries_points[];

    public OffenseMasteries() {
        this.masteries_points = new String[20];
        for (int i = 0; i < 20; i++) {
            this.masteries_points[i] = "0";
        }
    }

    public void setMasteries_points(String[] masteries_points) {
        this.masteries_points = masteries_points;
    }
    
    
    public double getDouble_edge(double damage, boolean melee) {
        int value = Integer.parseInt(masteries_points[index.DOUBLE_EDGE.ordinal()]);
        return melee ? value*0.02*damage : value*0.015*damage;
    }

    public double getFury() {
        return Integer.parseInt(masteries_points[index.FURY.ordinal()]) * 1.25;
    }

    public double getSorcery() {
        return Integer.parseInt(masteries_points[index.SORCERY.ordinal()]) * 1.25;
    }

    public boolean getButcher() {
        return masteries_points[index.SORCERY.ordinal()].equals("1");
    }

    public boolean getExpose_weakness() {
        return masteries_points[index.EXPOSE_WEAKNESS.ordinal()].equals("1");
    }

    public double getBrute_force(int level) {
        return Integer.parseInt(masteries_points[index.BRUTE_FORCE.ordinal()]) * 0.22 * level;
    }

    public double getMental_force(int level) {
        return Integer.parseInt(masteries_points[index.MENTAL_FORCE.ordinal()]) * 0.3 * level;
    }

    public boolean getFeast() {
        return masteries_points[index.FEAST.ordinal()].equals("1");
    }

    public boolean getSpell_weaving() {
        return masteries_points[index.SPELL_WEAVING.ordinal()].equals("1");
    }

    public double getMartial_mastery() {
        return Integer.parseInt(masteries_points[index.MARTIAL_MASTERY.ordinal()]) * 4;
    }

    public double getArcane_mastery() {
        return Integer.parseInt(masteries_points[index.ARCANE_MASTERY.ordinal()]) * 6;
    }

    public double getExecutioner(double damage, double hp_porcent) {
        int value = Integer.parseInt(masteries_points[index.EXECUTIONER.ordinal()]);
        return value == 0 ? 0 : (value*.15 + .05) > hp_porcent ? damage*0.05 : 0;
    }

    public boolean getBlade_weaving() {
        return masteries_points[index.BLADE_WEAVING.ordinal()].equals("1");
    }

    public double getWarlord() {
        int value = Integer.parseInt(masteries_points[index.WARLORD.ordinal()]);
        return value == 0 ? 0 : value * 0.015 + 0.005;
    }

    public double getArchemage() {
        int value = Integer.parseInt(masteries_points[index.ARCHEMAGE.ordinal()]);
        return value == 0 ? 0 : value * 0.015 + 0.005;
    }

    public boolean getDangeorus_game() {
        return masteries_points[index.DANGEORUS_GAME.ordinal()].equals("1");
    }

    public double getFrenzy(double atkspped) {
        return Integer.parseInt(masteries_points[index.FRENZY.ordinal()]) * 0.05 * atkspped;
    }

    public double getDevasting_strike() {
        return Integer.parseInt(masteries_points[index.DEVASTING_STRIKE.ordinal()]) * 0.02;
    }

    public double getArcane_blade(double ap) {
        return Integer.parseInt(masteries_points[index.ARCANE_BLADE.ordinal()]) * 0.05;
    }

    public double getHavoc(double damage) {
        return Integer.parseInt(masteries_points[index.HAVOC.ordinal()]) * 0.03;
    }    
    
    
    
}
