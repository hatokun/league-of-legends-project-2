/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package masteries;

/**
 *
 * @author ismael tesisteco
 */
public class UtilityMasteries {
    
    enum index {PHASE_WALKER, FLEET_FOOT, MEDITATION, SCOUT, SUMMONERS_INSIGHT, 
                STRENGTH_SPIRIT, ALCHEMIST, GREED, RUNIC_AFFINITY, VAMPIRISM,
                CULINARY_MASTER, SCAVENGER, WEALTH, EXAPANDED_MIND, INSIPRATION,
                BANDIT, INTELLIGENCE, WANDERER};
    
    private String masteries_points[];

    public boolean getPhase_walker() {
        return masteries_points[index.PHASE_WALKER.ordinal()].equals("1");
    }

    public double getFleet_foot(double speed) {
        return Integer.parseInt(masteries_points[index.FLEET_FOOT.ordinal()]) * 0.005 * speed;
    }

    public int getMeditation() {
        return Integer.parseInt(masteries_points[index.MEDITATION.ordinal()]);
    }

    public boolean getScout() {
         return masteries_points[index.SCOUT.ordinal()].equals("1");
    }

    public double getSummoners_insight() {
        int value = Integer.parseInt(masteries_points[index.SUMMONERS_INSIGHT.ordinal()]);
        return value == 0 ? value : 0.01 + value * 0.03;
    }

    public double getStrength_spirit(double mana) {
        return Integer.parseInt(masteries_points[index.STRENGTH_SPIRIT.ordinal()]) * 0.03 * mana;
    }

    public boolean getAlchemist() {
        return masteries_points[index.ALCHEMIST.ordinal()].equals("1");
    }

    public double getGreed() {
        return Integer.parseInt(masteries_points[index.GREED.ordinal()]) * 0.5;
    }

    public boolean getRunic_affinity() {
        return masteries_points[index.RUNIC_AFFINITY.ordinal()].equals("1");
    }

    public double getVampirism() {
        return Integer.parseInt(masteries_points[index.VAMPIRISM.ordinal()])*0.01;
    }

    public boolean getCulinary_master() {
        return masteries_points[index.CULINARY_MASTER.ordinal()].equals("1");
    }

    public boolean getScavenger() {
        return masteries_points[index.SCAVENGER.ordinal()].equals("1");
    }

    public int getWealth() {
        return Integer.parseInt(masteries_points[index.WEALTH.ordinal()])*40;
    }

    public double getExapanded_mind() {
        int value = Integer.parseInt(masteries_points[index.EXAPANDED_MIND.ordinal()]);
        return value == 0 ? value : 0.005 + value * 0.015;
    }

    public int getInsipration() {
        return Integer.parseInt(masteries_points[index.INSIPRATION.ordinal()])*5;
    }

    public boolean getBandit() {
        return masteries_points[index.BANDIT.ordinal()].equals("1");
    }

    public double getIntelligence() {
        int value = Integer.parseInt(masteries_points[index.INTELLIGENCE.ordinal()]);
        return value == 0 ? value : 0.005 + value * 0.015;
    }

    public double getWanderer(double speed) {
        return Integer.parseInt(masteries_points[index.FLEET_FOOT.ordinal()]) * 0.05 * speed;
    }   

    public UtilityMasteries() {
        this.masteries_points = new String[20];
        for (int i = 0; i < 20; i++) {
            this.masteries_points[i] = "0";
        }
    }

    public void setMasteries_points(String[] masteries_points) {
        this.masteries_points = masteries_points;
    }       
    
}
