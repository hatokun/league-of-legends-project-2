/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package interfaces;

import Runes.RunePage;
import masteries.DefenseMasteries;
import masteries.MasteriesPage;
import masteries.OffenseMasteries;
import masteries.UtilityMasteries;
import util.Items;
import util.Skills;
import util.Statistics;

/**
 *
 * @author ismael tesisteco
 */
public abstract class Champion implements Unit{
    
    protected Skills skills;
    protected Statistics statistics;
    protected MasteriesPage masteries;
    //protected OffenseMasteries offenseMasteries;
    //protected DefenseMasteries defenseMasteries;
    //protected UtilityMasteries utilityMasteries;

    protected RunePage runes;
    protected Items items;    

    
    public void setOffenseMasteries(OffenseMasteries offenseMasteries) {
        this.masteries.setOffenseMasteries(offenseMasteries);
    }

    public void setDefenseMasteries(DefenseMasteries defenseMasteries) {
        this.masteries.setDefenseMasteries(defenseMasteries);
    }
    
    public void setUtilityMasteries(UtilityMasteries utilityMasteries) {
        this.masteries.setUtilityMasteries(utilityMasteries);
    }       
    
    private OffenseMasteries getOffenseMasteries(){
        return this.masteries.getOffenseMasteries();
    }
    
    private DefenseMasteries getDefenseMasteries(){
        return this.masteries.getDefenseMasteries();
    }  
    
    private UtilityMasteries getUtilityMasteries(){
        return this.masteries.getUtilityMasteries();
    }      
    
    public abstract String getPassive_information();
    public abstract String getQ_information();
    public abstract String getW_information();
    public abstract String getE_information();
    public abstract String getR_information();
    
    public abstract double getQ_damage(Unit unit);
    public abstract double getW_damage();
    public abstract double getE_damage();
    public abstract double getR_damage();    
    public abstract double getA_damage();
    
    
    
    
    
    protected double getMagicReduction(Unit unit){
    
        double magic_penetration = this.getMagicPenetration();
        double magic_penetration_flat = this.getMagicPenetration();
        
        double mr = unit.getMagicResistence()*magic_penetration - magic_penetration_flat;
        double mr_reduction = 1 - (mr / (100 + mr));
        
        return  mr_reduction;    
    }
    
    protected double getRealDamage(double damage, Unit unit){
    
        double extra_damage = this.masteries.getOffenseMasteries().getDouble_edge(damage, statistics.isMelee());
        if(unit.isChampion()){            
            extra_damage += this.masteries.getOffenseMasteries().getExecutioner(damage, unit.getHP_percent());
            if(((Champion)unit).getOffenseMasteries().getDouble_edge(1, unit.getStatistics().isMelee()) != 0 )
                extra_damage += unit.getStatistics().isMelee() ? damage*0.01 : damage*0.015;
        }        
        extra_damage += this.masteries.getOffenseMasteries().getHavoc(damage);
       
        extra_damage -= ((Champion)unit).getDefenseMasteries().getUnyielding(unit.getStatistics().isMelee());
        
      
        
        return  damage + extra_damage;    
    }    

 
    
    @Override
    public double getHP_percent(){
        return statistics.getActual_health()/statistics.getHealth();
    }
        
        
    @Override
    public boolean isChampion(){
        return true;
    }
    
    public void setSkills(Skills skills){
        this.skills = skills;
    }
 
    @Override
    public double getAP(){
        double ap = 47;
        return ap;
    }
    
    @Override
    public double getHP() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getArmor() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getMagicResistence() {
        return this.statistics.getMagic_resistence();
    }

    @Override
    public double getMagicPenetration() {
        return 1 - this.masteries.getOffenseMasteries().getDevasting_strike();
    }

    @Override
    public double getMagicPenetrationFlat() {
        return 0;
    }

    @Override
    public double getMagicResistenceReduction() {
        return 0;
    }

    @Override
    public double getMagicResistenceReductionFlat() {
        return 0;
    }

    @Override
    public double getArmorPenetration() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getArmorPenetrationFlat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getArmorReduction() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getArmorReductionFlat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }   
    
    public void setLevel(int level){
        this.statistics.setLevel(level);
    }
    
}
