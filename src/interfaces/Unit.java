/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package interfaces;

import util.Statistics;

/**
 *
 * @author ismael tesisteco
 */
public interface Unit {
    Statistics getStatistics();
    boolean isChampion();
    double getHP();
    double getHP_percent();
    double getArmor();
    double getMagicResistence();
    double getAP();
    double getMagicPenetration();
    double getMagicPenetrationFlat();
    double getMagicResistenceReduction();
    double getMagicResistenceReductionFlat();
    double getArmorPenetration();
    double getArmorPenetrationFlat();
    double getArmorReduction();
    double getArmorReductionFlat();
}
