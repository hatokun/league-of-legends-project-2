/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Runes;

/**
 *
 * @author ismael tesisteco
 */
public class Marks {
    enum runes{ AbilityPower, AbilityPowerScaling, Armor, ArmorScaling, ArmorPenetration,
                AttackDamage, AttackDamageScaling, AttackSpeed, CooldDownReduction, 
                CooldDownReductionScaling, CriticalChance, CriticalDamage, Energy,
                EnergyScaling, EnergyRegeneration, EnergyRegenerationScaling, Experience,
                Gold, Health, HealthScaling, PercentHealth, HealthRegeneration, 
                HealthRegenerationScaling, HybridPenetration, LifeSteal, MagicResist, MagicResistScaling,
                Mana, ManaScaling, ManaRegeneration, ManaRegenerationScaling, MoventSpeed,
                Revival, SpellVamp};

    private String runesArray[];
    
    public double getAbilityPower() {
        return Integer.parseInt(runesArray[runes.AbilityPower.ordinal()])*0.59;
    }

    public double getAbilityPowerScaling() {
        return Integer.parseInt(runesArray[runes.AbilityPowerScaling.ordinal()])*(1.8/18.0);
    }

    public double getArmor() {
        return Integer.parseInt(runesArray[runes.Armor.ordinal()])*0.91;
    }

    public double getArmorScaling() {
        return Integer.parseInt(runesArray[runes.ArmorScaling.ordinal()])*0.0;
    }

    public double getArmorPenetration() {
        return Integer.parseInt(runesArray[runes.ArmorPenetration.ordinal()])*1.28;
    }

    public double getAttackDamage() {
        return Integer.parseInt(runesArray[runes.AttackDamage.ordinal()])*0.95;
    }

    public double getAttackDamageScaling() {
        return Integer.parseInt(runesArray[runes.AttackDamageScaling.ordinal()])*(2.43/18.0);
    }

    public double getAttackSpeed() {
        return Integer.parseInt(runesArray[runes.AttackSpeed.ordinal()])*1.7/100.0;
    }

    public double getCooldDownReduction() {
        return Integer.parseInt(runesArray[runes.CooldDownReduction.ordinal()])*0.2/100.0;
    }

    public double getCooldDownReductionScaling() {
        return Integer.parseInt(runesArray[runes.CooldDownReductionScaling.ordinal()])*0.0;
    }

    public double getCriticalChance() {
        return Integer.parseInt(runesArray[runes.CriticalChance.ordinal()])*0.93/100.0;
    }

    public double getCriticalDamage() {
        return Integer.parseInt(runesArray[runes.CriticalDamage.ordinal()])*2.23/100.0;
    }

    public double getEnergy() {
        return Integer.parseInt(runesArray[runes.Energy.ordinal()])*0.0;
    }

    public double getEnergyScaling() {
        return Integer.parseInt(runesArray[runes.EnergyScaling.ordinal()])*0.0;
    }

    public double getEnergyRenegeration() {
        return Integer.parseInt(runesArray[runes.EnergyRegeneration.ordinal()])*0.0;
    }

    public double getEnergyRegenerationScaling() {
        return Integer.parseInt(runesArray[runes.EnergyRegenerationScaling.ordinal()])*0.0;
    }

    public double getExperience() {
        return Integer.parseInt(runesArray[runes.Experience.ordinal()])*0.0;
    }

    public double getGold() {
        return Integer.parseInt(runesArray[runes.Gold.ordinal()])*0.0;
    }

    public double getHealth() {
        return Integer.parseInt(runesArray[runes.Health.ordinal()])*3.47;
    }

    public double getHealthScaling() {
        return Integer.parseInt(runesArray[runes.HealthScaling.ordinal()])*(9.72/18.0);
    }

    public double getPercentHealth() {
        return Integer.parseInt(runesArray[runes.PercentHealth.ordinal()])*0;
    }

    public double getHealthRegeneration() {
        return Integer.parseInt(runesArray[runes.HealthRegeneration.ordinal()])*0;
    }

    public double getHealthRegenerationScaling() {
        return Integer.parseInt(runesArray[runes.HealthRegenerationScaling.ordinal()])*0;
    }

    public double getHybridPenetration() {
        return Integer.parseInt(runesArray[runes.HybridPenetration.ordinal()]);
    }

    public double getLifeSteal() {
        return Integer.parseInt(runesArray[runes.LifeSteal.ordinal()])*0;
    }      
    
    public double getMagicResist() {
        return Integer.parseInt(runesArray[runes.MagicResist.ordinal()])*0.77;
    }

    public double getMagicResitScaling() {
        return Integer.parseInt(runesArray[runes.MagicResistScaling.ordinal()])*(1.26/18.0);
    }

    public double getMana() {
        return Integer.parseInt(runesArray[runes.Mana.ordinal()])*5.91;
    }

    public double getManaScaling() {
        return Integer.parseInt(runesArray[runes.ManaScaling.ordinal()])*(21.06/18.0);
    }

    public double getManaRegeneration() {
        return Integer.parseInt(runesArray[runes.ManaRegeneration.ordinal()])*0.26;
    }

    public double getManaRegenerationScaling() {
        return Integer.parseInt(runesArray[runes.ManaRegenerationScaling.ordinal()])*(0.0/18.0);
    }

    public double getMoventSpeed() {
        return Integer.parseInt(runesArray[runes.MoventSpeed.ordinal()])*0.0;
    }

    public double getRevival() {
        return Integer.parseInt(runesArray[runes.Revival.ordinal()])*0.0;
    }

    public double getSpellVamp() {
        return Integer.parseInt(runesArray[runes.SpellVamp.ordinal()])*0.0;
    }
    
}
