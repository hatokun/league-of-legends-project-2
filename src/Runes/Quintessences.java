/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Runes;

/**
 *
 * @author ismael tesisteco
 */
public class Quintessences {
    enum runes{ AbilityPower, AbilityPowerScaling, Armor, ArmorScaling, ArmorPenetration,
                AttackDamage, AttackDamageScaling, AttackSpeed, CooldDownReduction, 
                CooldDownReductionScaling, CriticalChance, CriticalDamage, Energy,
                EnergyScaling, EnergyRegeneration, EnergyRegenerationScaling, Experience,
                Gold, Health, HealthScaling, PercentHealth, HealthRegeneration, 
                HealthRegenerationScaling, HybridPenetration, LifeSteal, MagicResist, MagicResistScaling,
                Mana, ManaScaling, ManaRegeneration, ManaRegenerationScaling, MoventSpeed,
                Revival, SpellVamp};

    private String runesArray[];
    
    public double getAbilityPower() {
        return Integer.parseInt(runesArray[runes.AbilityPower.ordinal()])*4.95;
    }

    public double getAbilityPowerScaling() {
        return Integer.parseInt(runesArray[runes.AbilityPowerScaling.ordinal()])*(7.74/18.0);
    }

    public double getArmor() {
        return Integer.parseInt(runesArray[runes.Armor.ordinal()])*4.26;
    }

    public double getArmorScaling() {
        return Integer.parseInt(runesArray[runes.ArmorScaling.ordinal()])*(6.84/18.0);
    }

    public double getArmorPenetration() {
        return Integer.parseInt(runesArray[runes.ArmorPenetration.ordinal()])*2.56;
    }

    public double getAttackDamage() {
        return Integer.parseInt(runesArray[runes.AttackDamage.ordinal()])*2.25;
    }

    public double getAttackDamageScaling() {
        return Integer.parseInt(runesArray[runes.AttackDamageScaling.ordinal()])*(4.5/18.0);
    }

    public double getAttackSpeed() {
        return Integer.parseInt(runesArray[runes.AttackSpeed.ordinal()])*4.5/100.0;
    }

    public double getCooldDownReduction() {
        return Integer.parseInt(runesArray[runes.CooldDownReduction.ordinal()])*2.5/100.0;
    }

    public double getCooldDownReductionScaling() {
        return Integer.parseInt(runesArray[runes.CooldDownReductionScaling.ordinal()])*(5.0/18.0)/100.0;
    }

    public double getCriticalChance() {
        return Integer.parseInt(runesArray[runes.CriticalChance.ordinal()])*1.86/100.0;
    }

    public double getCriticalDamage() {
        return Integer.parseInt(runesArray[runes.CriticalDamage.ordinal()])*4.46/100.0;
    }

    public double getEnergy() {
        return Integer.parseInt(runesArray[runes.Energy.ordinal()])*5.4;
    }

    public double getEnergyScaling() {
        return Integer.parseInt(runesArray[runes.EnergyScaling.ordinal()])*0.0;
    }

    public double getEnergyRenegeration() {
        return Integer.parseInt(runesArray[runes.EnergyRegeneration.ordinal()])*1.575;
    }

    public double getEnergyRegenerationScaling() {
        return Integer.parseInt(runesArray[runes.EnergyRegeneration.ordinal()])*0.0;
    }

    public double getExperience() {
        return Integer.parseInt(runesArray[runes.Experience.ordinal()])*2.0/100.0;
    }

    public double getGold() {
        return Integer.parseInt(runesArray[runes.Gold.ordinal()])*1.0;
    }

    public double getHealth() {
        return Integer.parseInt(runesArray[runes.Health.ordinal()])*26.0;
    }

    public double getHealthScaling() {
        return Integer.parseInt(runesArray[runes.HealthScaling.ordinal()])*(48.6/18.0);
    }

    public double getPercentHealth() {
        return Integer.parseInt(runesArray[runes.PercentHealth.ordinal()])*1.5/100.0;
    }

    public double getHealthRegeneration() {
        return Integer.parseInt(runesArray[runes.HealthRegeneration.ordinal()])*2.7;
    }

    public double getHealthRegenerationScaling() {
        return Integer.parseInt(runesArray[runes.HealthRegenerationScaling.ordinal()])*(5.04/18.0);
    }

    public double getHybridPenetration() {
        return Integer.parseInt(runesArray[runes.HybridPenetration.ordinal()]);
    }

    public double getLifeSteal() {
        return Integer.parseInt(runesArray[runes.LifeSteal.ordinal()])*1.5/100.0;
    }      
    
    public double getMagicResist() {
        return Integer.parseInt(runesArray[runes.MagicResist.ordinal()])*4.0;
    }

    public double getMagicResitScaling() {
        return Integer.parseInt(runesArray[runes.MagicResistScaling.ordinal()])*(6.66/18.0);
    }

    public double getMana() {
        return Integer.parseInt(runesArray[runes.Mana.ordinal()])*37.5;
    }

    public double getManaScaling() {
        return Integer.parseInt(runesArray[runes.ManaScaling.ordinal()])*(75.06/18.0);
    }

    public double getManaRegeneration() {
        return Integer.parseInt(runesArray[runes.ManaRegeneration.ordinal()])*1.25;
    }

    public double getManaRegenerationScaling() {
        return Integer.parseInt(runesArray[runes.ManaRegenerationScaling.ordinal()])*(4.32/18.0);
    }

    public double getMoventSpeed() {
        return Integer.parseInt(runesArray[runes.MoventSpeed.ordinal()])*1.5/100.0;
    }

    public double getRevival() {
        return Integer.parseInt(runesArray[runes.Revival.ordinal()])*5.0/100.0;
    }

    public double getSpellVamp() {
        return Integer.parseInt(runesArray[runes.SpellVamp.ordinal()])*2.0/100.0;
    }
}
