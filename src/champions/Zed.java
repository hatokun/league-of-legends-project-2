/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package champions;

import interfaces.Champion;
import interfaces.Unit;
import util.Statistics;

/**
 *
 * @author ismael tesisteco
 */
public class Zed extends Champion{
    
    public Zed() {
        this.statistics = new Statistics();
        this.statistics.setHealth(445);
        this.statistics.setHealth_per_lv(80);
        this.statistics.setHealth_regen(6);
        this.statistics.setHealth_regen_per_lv(0.65);
        this.statistics.setEnergy(200);
        this.statistics.setEnergy_regen(50);
        this.statistics.setAttack_damage(49);
        this.statistics.setAttack_damage_per_lv(3.4);
        this.statistics.setAttack_speed(0.658);
        this.statistics.setAttack_speed_per_lv(0.031);        
        this.statistics.setArmor(21);
        this.statistics.setArmor_per_lv(3.5);
        this.statistics.setMagic_resistence(30);
        this.statistics.setMagic_resistence_per_lv(1.25);
        this.statistics.setMovent_speed(345); 
        this.statistics.setMelee();
    }    

    @Override
    public String getPassive_information() {
        return "Contempt for the Weak";
    }

    @Override
    public String getQ_information() {
        return "Razor Shuriken";
    }

    @Override
    public String getW_information() {
        return "Living Shadow";
    }

    @Override
    public String getE_information() {
        return "Shadow Slash";
    }

    @Override
    public String getR_information() {
        return "Death Mark";
    }

    @Override
    public double getQ_damage(Unit unit) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getW_damage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getE_damage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getR_damage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getA_damage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Statistics getStatistics() {
        return this.statistics;
    }
    
}
