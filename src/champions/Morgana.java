/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package champions;

import interfaces.Champion;
import interfaces.Unit;
import masteries.OffenseMasteries;
import util.*;

/**
 *
 * @author ismael tesisteco
 */
public class Morgana extends Champion{

    public Morgana() {
        this.statistics = new Statistics();
        this.statistics.setHealth(403);
        this.statistics.setHealth_per_lv(86);
        this.statistics.setHealth_regen(4.7);
        this.statistics.setHealth_regen_per_lv(0.6);
        this.statistics.setMana(240);
        this.statistics.setMana_per_lv(60);
        this.statistics.setMana_regen(6.8);
        this.statistics.setMana_regen_per_lv(0.65);
        this.statistics.setAttack_damage(51.38);
        this.statistics.setAttack_damage_per_lv(3.5);
        this.statistics.setAttack_speed(0.625);
        this.statistics.setAttack_speed_per_lv(0.0153);
        this.statistics.setArmor(19);
        this.statistics.setArmor_per_lv(3.8);
        this.statistics.setMagic_resistence(30);
        this.statistics.setMagic_resistence_per_lv(0);
        this.statistics.setMovent_speed(335);           
    }
    
    

    @Override
    public String getPassive_information() {
        int porcent = 10;
        /*
        switch (this.statistics.getLevel()) {
            case 7:
                porcent = 15;
                break;
            case 13:
                porcent = 20;                
        }/**/
        
        String inf = "Soul Siphon";
        
        
        return inf;                                                     
    }
    
    @Override
    public String getQ_information() {
        String inf = "Dark Binding";                
        return inf;    
    }

    @Override
    public String getW_information() {
        String inf = "Tormented Soil";                
        return inf;  
    }

    @Override
    public String getE_information() {
        String inf = "Black Shield";                
        return inf;  
    }

    @Override
    public String getR_information() {
        String inf = "Soul Shackles";                
        return inf;  
    }

    @Override
    public Statistics getStatistics() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.                
    }

    @Override
    public double getQ_damage(Unit unit) {
        double ap = this.getAP();
        int lv = this.skills.getlvQ();
        double damage = (25 + lv*55 + .9*ap)*this.getMagicReduction(unit);
        return  lv == 0 ? 0 : this.getRealDamage(damage, unit);
    }

    @Override
    public double getW_damage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getE_damage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getR_damage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getA_damage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }  
    
    
}
