/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package champions;

import interfaces.Champion;
import interfaces.Unit;
import util.Statistics;

/**
 *
 * @author ismael tesisteco
 */
public class Janna extends Champion{

    public Janna() {
        this.statistics = new Statistics();
        this.statistics.setHealth(356);
        this.statistics.setArmor(13);
        this.statistics.setMagic_resistence(30);
    }
    
    

    @Override
    public String getPassive_information() {
        return "Tailwind";
    }

    @Override
    public String getQ_information() {
        return "Howling Gale";
    }

    @Override
    public String getW_information() {
        return "Zephyr";
    }

    @Override
    public String getE_information() {
        return "Eye Of The Storm";
    }

    @Override
    public String getR_information() {
        return "Monsoon";
    }

    @Override
    public double getQ_damage(Unit unit) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getW_damage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getE_damage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getR_damage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getA_damage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Statistics getStatistics() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getHP() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getArmor() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getMagicResistence() {
        double mr_base = this.statistics.getMagic_resistence();
        double mr_runes = this.statistics.getMagic_resistence();
        double mr_masteries = this.statistics.getMagic_resistence();
        return mr_base;
        //return mr_base + mr_runes + mr_masteries;
    }

    @Override
    public double getAP() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getMagicPenetration() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getMagicPenetrationFlat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getMagicResistenceReduction() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getMagicResistenceReductionFlat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getArmorPenetration() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getArmorPenetrationFlat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getArmorReduction() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getArmorReductionFlat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
