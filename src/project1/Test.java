/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project1;

import champions.Janna;
import champions.Morgana;
import champions.Zed;
import interfaces.Champion;
import masteries.*;
import util.Skills;

/**
 *
 * @author ismael tesisteco
 */
public class Test {
    public static void main(String[] args) {
        Champion morgana = new Morgana();
        OffenseMasteries offm = new OffenseMasteries();
        DefenseMasteries defm = new DefenseMasteries();
        UtilityMasteries utlm = new UtilityMasteries();
        String offmMorgana[] = {"1","0","4","0",
                               "0","0","3","0",
                               "0","0","1","3",
                               "0","0","3","1",
                               "0","3",    "1",   
                                   "1"};
        String defmMorgana[] = {"0","0","0","0",
                                "0","0",    "0",
                                "0","0","0","0",
                                "0","0","0","0",
                                "0","0","0",   
                                    "0"};
        String utlmMorgana[] = {"0","1","3","0",
                                    "3","0","1",
                                "0","0","0","1",
                                "0","0","0","0",
                                    "0","0",   
                                    "0"};        
        
        offm.setMasteries_points(offmMorgana);
        defm.setMasteries_points(defmMorgana);
        utlm.setMasteries_points(utlmMorgana);
        morgana.setOffenseMasteries(offm);
        morgana.setDefenseMasteries(defm);
        morgana.setUtilityMasteries(utlm);
        
        Skills skills = new Skills();
        skills.setlvQ(1);
        morgana.setSkills(skills);
        
        
        Champion zed = new Zed();
        OffenseMasteries offmz = new OffenseMasteries();
        DefenseMasteries defmz = new DefenseMasteries();
        UtilityMasteries utlmz = new UtilityMasteries();
        String offmZed[] =    {"1","4","0","1",
                               "0","3","0","0",
                               "0","1","0","3",
                               "0","3","0","1",
                               "0","3",    "0",   
                                   "1"};
        String defmZed[] =     {"2","2","0","0",
                                "1","3",    "0",
                                "0","1","0","0",
                                "0","0","0","0",
                                "0","0","0",   
                                    "0"};
        String utlmZed[] =     {"0","0","0","0",
                                    "0","0","0",
                                "0","0","0","0",
                                "0","0","0","0",
                                    "0","0",   
                                    "0"};        
        
        offmz.setMasteries_points(offmZed);
        defmz.setMasteries_points(defmZed);
        utlmz.setMasteries_points(utlmZed);
        zed.setOffenseMasteries(offm);
        zed.setDefenseMasteries(defm);
        zed.setUtilityMasteries(utlm); 
        zed.setLevel(1);
        
        System.out.println(morgana.getQ_information() + " damage over zed:" + morgana.getQ_damage(zed) );
        
    }
}
