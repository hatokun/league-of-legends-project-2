/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project1;

import interfaces.Champion;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SpinnerNumberModel;
import util.FactoryUnits;

/**
 *
 * @author ismael tesisteco
 */
public class Builder extends javax.swing.JFrame {

    
    private Champion champion;
    private int contaSkills = 1;
    
    /**
     * Creates new form Builder
     */
    @SuppressWarnings("empty-statement")
    public Builder() {
        initComponents();
        
        this.initFrame();
        
        File folder = new File("src/champions");    
        int conta = 0;
        for (File f : folder.listFiles()) {
            String nameChampion = f.getName().substring(0, f.getName().length()-5);
            this.jCBChampion.insertItemAt(nameChampion, conta++);
        }
        
        this.jCBChampion.setSelectedIndex(0);
        
        this.initSkills();
        
           
        
    }
    
    private void initFrame(){
        //frame size
        this.setBounds(150, 100, 500, 530);
        this.setResizable(false);
        
        //champion img
        this.jLabelChampionImg.setBounds(10, 10, 120, 120);
        this.jLabelChampionImg1.setBounds(365, 10, 120, 120);
        
        //champion select
        this.jLabelChampion.setBounds(150, 10, 100, 25);
        this.jCBChampion.setBounds(250, 10, 100, 25);
        
        //champion lv select
        this.jLabelChampionlv.setBounds(150, 60, 100, 25);
        this.jCBChampionlv.setBounds(250, 60, 100, 25); 
                
        //masteries and runes
        this.jbMasteries.setBounds(145, 100, 100, 30);
        this.jbRunes.setBounds(250, 100, 100, 30);
        
        //skills img
        this.jLabelPassive.setBounds(10, 150, 64, 64);
        this.jLabelQ.setBounds(110, 150, 64, 64);
        this.jLabelW.setBounds(210, 150, 64, 64);
        this.jLabelE.setBounds(310, 150, 64, 64);
        this.jLabelR.setBounds(410, 150, 64, 64);          
        
        //spinner skills lv
        this.jslvq.setBounds(122, 240, 40, 25);
        this.jslvw.setBounds(222, 240, 40, 25);
        this.jslve.setBounds(322, 240, 40, 25);
        this.jslvr.setBounds(422, 240, 40, 25);   
        //model skills
        int maxlvSkill = (this.jCBChampionlv.getSelectedIndex() + 1)/2 + 1;
        SpinnerNumberModel model = new SpinnerNumberModel(0, 0, maxlvSkill , 1);  
        this.jslvq.setModel(model);
        model = new SpinnerNumberModel(0, 0, maxlvSkill , 1);  
        this.jslvw.setModel(model);
        model = new SpinnerNumberModel(0, 0, maxlvSkill , 1);  
        this.jslve.setModel(model);
        model = new SpinnerNumberModel(0, 0, maxlvSkill , 1);  
        this.jslvr.setModel(model);
        
        //name of skills
        this.jLabelPassivename.setBounds(10, 215, 100, 25);
        this.jLabelQname.setBounds(110, 215, 100, 25);
        this.jLabelWname.setBounds(210, 215, 100, 25);
        this.jLabelEname.setBounds(310, 215, 100, 25);
        this.jLabelRname.setBounds(410, 215, 100, 25);        
        

                
      
    
    }
    
    
    private void initSkills(){
    
        String name = (String)this.jCBChampion.getSelectedItem();
        champion = (Champion)(FactoryUnits.getFactory()).getUnit(name);
        
       
        this.jLabelPassive.setToolTipText(champion.getPassive_information());
        this.jLabelQ.setToolTipText(champion.getQ_information());
        this.jLabelW.setToolTipText(champion.getW_information());
        this.jLabelE.setToolTipText(champion.getE_information());
        this.jLabelR.setToolTipText(champion.getR_information());
    
        this.setImage(name+"/passive.jpg", this.jLabelPassive);
        this.setImage(name+"/q.jpg", this.jLabelQ);
        this.setImage(name+"/w.jpg", this.jLabelW);
        this.setImage(name+"/e.jpg", this.jLabelE);
        this.setImage(name+"/r.jpg", this.jLabelR);
        this.setImage(name+"/champion.png", this.jLabelChampionImg);
        this.setImage(name+"/champion.png", this.jLabelChampionImg1);
        
        
        
        
        this.jLabelPassivename.setText(champion.getPassive_information());
        this.jLabelQname.setText(champion.getQ_information());
        this.jLabelWname.setText(champion.getW_information());
        this.jLabelEname.setText(champion.getE_information());
        this.jLabelRname.setText(champion.getR_information());        
        
    }
    
    
    private void setImage(String path, JLabel label){
        BufferedImage image = null;
        
        try {                
           image = ImageIO.read(new File("src/images/"+path));
        } catch (IOException ex) {
             // handle exception...
        }     
               
        label.setIcon(new ImageIcon(image));    
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jCBChampion = new javax.swing.JComboBox();
        jLabelChampion = new javax.swing.JLabel();
        jLabelChampionlv = new javax.swing.JLabel();
        jCBChampionlv = new javax.swing.JComboBox();
        jLabelPassive = new javax.swing.JLabel();
        jLabelQ = new javax.swing.JLabel();
        jLabelW = new javax.swing.JLabel();
        jLabelE = new javax.swing.JLabel();
        jLabelR = new javax.swing.JLabel();
        jslvq = new javax.swing.JSpinner();
        jslvw = new javax.swing.JSpinner();
        jslve = new javax.swing.JSpinner();
        jslvr = new javax.swing.JSpinner();
        jLabelQname = new javax.swing.JLabel();
        jLabelWname = new javax.swing.JLabel();
        jLabelEname = new javax.swing.JLabel();
        jLabelRname = new javax.swing.JLabel();
        jLabelPassivename = new javax.swing.JLabel();
        jLabelChampionImg = new javax.swing.JLabel();
        jbMasteries = new javax.swing.JButton();
        jbRunes = new javax.swing.JButton();
        jLabelChampionImg1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBounds(new java.awt.Rectangle(0, 0, 64, 64));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setMinimumSize(new java.awt.Dimension(64, 64));
        getContentPane().setLayout(null);

        jCBChampion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBChampionActionPerformed(evt);
            }
        });
        getContentPane().add(jCBChampion);
        jCBChampion.setBounds(220, 30, 105, 20);

        jLabelChampion.setText("Champion:");
        getContentPane().add(jLabelChampion);
        jLabelChampion.setBounds(110, 40, 104, 14);

        jLabelChampionlv.setText("Champion lv:");
        getContentPane().add(jLabelChampionlv);
        jLabelChampionlv.setBounds(110, 70, 104, 14);

        jCBChampionlv.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18" }));
        jCBChampionlv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBChampionlvActionPerformed(evt);
            }
        });
        getContentPane().add(jCBChampionlv);
        jCBChampionlv.setBounds(220, 70, 105, 20);
        getContentPane().add(jLabelPassive);
        jLabelPassive.setBounds(10, 128, 104, 0);
        getContentPane().add(jLabelQ);
        jLabelQ.setBounds(10, 146, 104, 0);
        getContentPane().add(jLabelW);
        jLabelW.setBounds(10, 166, 104, 0);
        getContentPane().add(jLabelE);
        jLabelE.setBounds(10, 184, 104, 0);

        jLabelR.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabelRMouseMoved(evt);
            }
        });
        getContentPane().add(jLabelR);
        jLabelR.setBounds(10, 228, 104, 0);

        jslvq.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jslvqStateChanged(evt);
            }
        });
        jslvq.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                jslvqCaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                jslvqInputMethodTextChanged(evt);
            }
        });
        getContentPane().add(jslvq);
        jslvq.setBounds(20, 210, 40, 20);

        jslvw.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jslvwStateChanged(evt);
            }
        });
        getContentPane().add(jslvw);
        jslvw.setBounds(100, 210, 40, 20);
        getContentPane().add(jslve);
        jslve.setBounds(190, 210, 40, 20);
        getContentPane().add(jslvr);
        jslvr.setBounds(270, 210, 40, 20);
        getContentPane().add(jLabelQname);
        jLabelQname.setBounds(10, 146, 104, 0);
        getContentPane().add(jLabelWname);
        jLabelWname.setBounds(10, 146, 104, 0);
        getContentPane().add(jLabelEname);
        jLabelEname.setBounds(10, 146, 104, 0);
        getContentPane().add(jLabelRname);
        jLabelRname.setBounds(10, 146, 104, 0);
        getContentPane().add(jLabelPassivename);
        jLabelPassivename.setBounds(10, 146, 104, 0);
        getContentPane().add(jLabelChampionImg);
        jLabelChampionImg.setBounds(10, 400, 104, 14);

        jbMasteries.setText("Masteries");
        jbMasteries.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbMasteriesActionPerformed(evt);
            }
        });
        getContentPane().add(jbMasteries);
        jbMasteries.setBounds(110, 140, 100, 23);

        jbRunes.setText("Runes");
        jbRunes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbRunesActionPerformed(evt);
            }
        });
        getContentPane().add(jbRunes);
        jbRunes.setBounds(220, 140, 63, 23);
        getContentPane().add(jLabelChampionImg1);
        jLabelChampionImg1.setBounds(10, 400, 104, 14);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jCBChampionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBChampionActionPerformed
        // TODO add your handling code here:
        
        
        
        
        //this.jCBChampion.setSelectedIndex(this.jCBChampion.getSelectedIndex());
        initSkills();
        
        
        
        
        
    }//GEN-LAST:event_jCBChampionActionPerformed

    private void jCBChampionlvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBChampionlvActionPerformed
        // TODO add your handling code here:
        this.contaSkills = this.jCBChampionlv.getSelectedIndex() + 1;
        
        //model skills
        int maxlvSkill = (this.jCBChampionlv.getSelectedIndex())/2 + 1;
        
        SpinnerNumberModel model = new SpinnerNumberModel(0, 0, maxlvSkill , 1);  
        this.jslvq.setModel(model);
        model = new SpinnerNumberModel(0, 0, maxlvSkill , 1);  
        this.jslvw.setModel(model);
        model = new SpinnerNumberModel(0, 0, maxlvSkill , 1);  
        this.jslve.setModel(model);
        model = new SpinnerNumberModel(0, 0, maxlvSkill , 1);  
        this.jslvr.setModel(model);        
        
        
    }//GEN-LAST:event_jCBChampionlvActionPerformed

    private void jLabelRMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelRMouseMoved
        // TODO add your handling code here:

    }//GEN-LAST:event_jLabelRMouseMoved

    private void jbRunesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbRunesActionPerformed
        // TODO add your handling code here:
        MasteriesPanel masteries = new MasteriesPanel();        
        
    }//GEN-LAST:event_jbRunesActionPerformed

    private void jbMasteriesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbMasteriesActionPerformed
        // TODO add your handling code here:
MasteriesFrame mf = new MasteriesFrame()        ;
        
    }//GEN-LAST:event_jbMasteriesActionPerformed

    private void jslvqStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jslvqStateChanged
        // TODO add your handling code here:
        int pv = (this.jslvq.getPreviousValue() == null) ? -1 : (int)this.jslvq.getPreviousValue();
        
        if( (int)this.jslvq.getValue() >  pv){
            this.contaSkills--;
        }else{
            this.contaSkills++;
        }

        int des = (((int)this.jslvq.getValue()) == 1 && this.jCBChampionlv.getSelectedIndex() == 0)? 1 : ((int)this.jslvq.getValue())/2 + ((int)this.jslvw.getValue())/2 + ((int)this.jslve.getValue())/2 + ((int)this.jslvr.getValue())/2;
        //model skills
        int maxlvSkill = (this.jCBChampionlv.getSelectedIndex())/2 + 1 - des;
        System.out.println("desc = "+des);
        System.out.println("max = "+maxlvSkill);
        SpinnerNumberModel model = new SpinnerNumberModel((int)this.jslvw.getValue(), 0, maxlvSkill , 1);  
        this.jslvw.setModel(model);
        model = new SpinnerNumberModel((int)this.jslve.getValue(), 0, maxlvSkill , 1);  
        this.jslve.setModel(model);
        model = new SpinnerNumberModel((int)this.jslvr.getValue(), 0, maxlvSkill , 1);  
        this.jslvr.setModel(model);          
        
        
    }//GEN-LAST:event_jslvqStateChanged

    private void jslvqCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jslvqCaretPositionChanged
        // TODO add your handling code here:
        System.out.println(this.jslvq.getValue());
    }//GEN-LAST:event_jslvqCaretPositionChanged

    private void jslvqInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jslvqInputMethodTextChanged
        // TODO add your handling code here:
        System.out.println(this.jslvq.getValue());
    }//GEN-LAST:event_jslvqInputMethodTextChanged

    private void jslvwStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jslvwStateChanged
        // TODO add your handling code here:
        int pv = (this.jslvw.getPreviousValue() == null) ? -1 : (int)this.jslvw.getPreviousValue();
        
        if( (int)this.jslvw.getValue() >  pv){
            this.contaSkills--;
        }else{
            this.contaSkills++;
        }

        int des = (((int)this.jslvw.getValue()) == 1 && this.jCBChampionlv.getSelectedIndex() == 0)? 1 : ((int)this.jslvq.getValue())/2 + ((int)this.jslvw.getValue())/2 + ((int)this.jslve.getValue())/2 + ((int)this.jslvr.getValue())/2;
        //model skills
        int maxlvSkill = (this.jCBChampionlv.getSelectedIndex())/2 + 1 - des;
        System.out.println("desc = "+des);
        System.out.println("max = "+maxlvSkill);
        SpinnerNumberModel model = new SpinnerNumberModel((int)this.jslvq.getValue(), 0, maxlvSkill , 1);  
        this.jslvq.setModel(model);
        model = new SpinnerNumberModel((int)this.jslve.getValue(), 0, maxlvSkill , 1);  
        this.jslve.setModel(model);
        model = new SpinnerNumberModel((int)this.jslvr.getValue(), 0, maxlvSkill , 1);  
        this.jslvr.setModel(model);             
    }//GEN-LAST:event_jslvwStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Builder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Builder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Builder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Builder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Builder().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox jCBChampion;
    private javax.swing.JComboBox jCBChampionlv;
    private javax.swing.JLabel jLabelChampion;
    private javax.swing.JLabel jLabelChampionImg;
    private javax.swing.JLabel jLabelChampionImg1;
    private javax.swing.JLabel jLabelChampionlv;
    private javax.swing.JLabel jLabelE;
    private javax.swing.JLabel jLabelEname;
    private javax.swing.JLabel jLabelPassive;
    private javax.swing.JLabel jLabelPassivename;
    private javax.swing.JLabel jLabelQ;
    private javax.swing.JLabel jLabelQname;
    private javax.swing.JLabel jLabelR;
    private javax.swing.JLabel jLabelRname;
    private javax.swing.JLabel jLabelW;
    private javax.swing.JLabel jLabelWname;
    private javax.swing.JButton jbMasteries;
    private javax.swing.JButton jbRunes;
    private javax.swing.JSpinner jslve;
    private javax.swing.JSpinner jslvq;
    private javax.swing.JSpinner jslvr;
    private javax.swing.JSpinner jslvw;
    // End of variables declaration//GEN-END:variables
}
