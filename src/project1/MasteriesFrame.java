/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project1;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author ismael tesisteco
 */
public class MasteriesFrame extends JFrame{

    private BufferedImage image;
    
    public MasteriesFrame() {
        this.setBounds(100,100,920,533);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        /*
        try {                
            image = ImageIO.read(new File("/src/images/Masteries/Masteries-S3.png"));
        } catch (IOException ex) {
            // handle exception...
        }   
        
        JPanel panel = new JPanel(){ 
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(image, 0, 0, null); // see javadoc for more info on the parameters            
            } };
        panel.setBounds(100,100,920,533);
        panel.setVisible(true);        
        this.getContentPane().add(panel);     /**/   
        this.setLayout(new BorderLayout());
        this.add(new MasteriesPanel(), BorderLayout.CENTER);        
    }
                      
}
